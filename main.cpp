/*
 * main.cpp
 *
 *  Created on: 25/04/2016
 *      Author: puc
 */

#include<avr/io.h>
#include<util/delay.h>

int pd = 7,
num = 9,
d1 = 0,d2 = 0,d3 =0;



void setup() {
	DDRB = 0b11111111;
	DDRC = 0b00111111;
	DDRD = 0b00111111;
}


/*
 * Método responsável por atribuir ao registrador o numero desejado
 * Usamos todas as portas do registrador PORTC e pegamos uma emprestada do PORTB
 * @return void {}
 * @param int
 */
void plot(int num) {

	switch(num){
			case 9:
				PORTC &= 0x00;
				PORTC |= 0b00010000;
				PORTB &= 0b11011111;
				break;
			case 8:
				PORTC &= 0x00;
				PORTB &= 0b11011111;
				break;
			case 7:
				PORTC &= 0x00;
				PORTC |= 0b11111000;
				PORTB |= 0b00100000;
				break;
			case 6:
				PORTC &= 0x00;
				PORTC |= 0b00000010;
				PORTB &= 0b11011111;
				break;
			case 5:
				PORTC &= 0x00;
				PORTC |= 0b00010010;
				PORTB &= 0b11011111;
				break;
			case 4:
				PORTC &= 0x00;
				PORTC |= 0b00011001;
				PORTB &= 0b11011111;
				break;
			case 3:
				PORTC &= 0x00;
				PORTC |= 0b00110000;
				PORTB &= 0b11011111;
				break;
			case 2:
				PORTC &= 0x00;
				PORTC |= 0b00100100;
				PORTB &= 0b11011111;
				break;
			case 1:
				PORTC &= 0x00;
				PORTC |= 0b11111001;
				PORTB |= 0b00100000;
				break;
			case 0 :
				PORTC &= 0x00;
				PORTB |= 0b00100000;
				break;
	}

}

void loop() {

	PORTD = 1 << pd--;
	int i;

	for ( i = 1; i <= num; i++) {
		plot(i);

		if(pd == 6){
			plot(d2++);
		}else{
			if(pd == )
		}
	}


	if(pd < 5) {
		PORTD = 0b00000000;
		pd = 7;
	}
	

  for (int digit1=0; digit1 < 10; digit1++) {
    for (int digit2=0; digit2 < 10; digit2++) {
      unsigned long startTime = millis();
      for (unsigned long elapsed=0; elapsed < 600; elapsed = millis() - startTime) {
        lightDigit1(numbers[digit1]);
        delay(5);
        lightDigit2(numbers[digit2]);
        delay(5);
      }
    }
  }
}

int main() {
	setup();

	while (true){
		loop();
	}
	return 0;
}


/*
 * main.cpp
 *
 *  Created on: 25/04/2016
 *      Author: puc
 */

#include<avr/io.h>
#include<util/delay.h>

void setup() {
	DDRB = 0b11111111;
	DDRC = 0b00111111;
	DDRD = 0b00111111;
}

/*
 * Método responsável por atribuir ao registrador o numero desejado
 * Usamos todas as portas do registrador PORTC e pegamos uma emprestada do PORTB
 * @return void {}
 * @param int
 */
void plot(int num) {

	switch (num) {
	case 9:
		PORTC &= 0x00;
		PORTC |= 0b00010000;
		PORTB &= 0b11011111;
		break;
	case 8:
		PORTC &= 0x00;
		PORTB &= 0b11011111;
		break;
	case 7:
		PORTC &= 0x00;
		PORTC |= 0b11111000;
		PORTB |= 0b00100000;
		break;
	case 6:
		PORTC &= 0x00;
		PORTC |= 0b00000010;
		PORTB &= 0b11011111;
		break;
	case 5:
		PORTC &= 0x00;
		PORTC |= 0b00010010;
		PORTB &= 0b11011111;
		break;
	case 4:
		PORTC &= 0x00;
		PORTC |= 0b00011001;
		PORTB &= 0b11011111;
		break;
	case 3:
		PORTC &= 0x00;
		PORTC |= 0b00110000;
		PORTB &= 0b11011111;
		break;
	case 2:
		PORTC &= 0x00;
		PORTC |= 0b00100100;
		PORTB &= 0b11011111;
		break;
	case 1:
		PORTC &= 0x00;
		PORTC |= 0b11111001;
		PORTB |= 0b00100000;
		break;
	case 0:
		PORTC &= 0x00;
		PORTB |= 0b00100000;
		break;
	}

}

void loop() {

	for (int i = 0; i < 9; i++) {
		for (int j = 0; j < 9; j++) {
			for (int h = 0; h < 9; h++) {

				PORTD = 1 << 5;
				plot(i);
				_delay_ms(1000);
				PORTD |= 0 << 5;

				PORTD = 1 << 6;
				plot(j);
				_delay_ms(1000);
				PORTD |= 0 << 6;

				PORTD = 1 << 7;
				plot(h);
				_delay_ms(1000);
				PORTD |= 0 << 7;

			}
		}
		_delay_ms(1000);

	}

}

int main() {
	setup();

	while (true) {
		loop();
	}
	return 0;
}



